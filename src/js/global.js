'use strict';

var site = site || {};
var WebFontConfig = {
    monotype: {
        projectId: '03703911-6238-4506-a46e-f205325a1370',
        version: 10111,
        loadAllFonts: true
    }
};

(function( d ) {
    var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
    wf.src = '//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js';
    s.parentNode.insertBefore( wf, s );
})( document );

jQuery( function( $ ) {

    site.elems = site.elems || {};
    site.elems.$scroll = $( 'html,body' );
    site.elems.$body = $( 'body' );
    site.elems.$header = $( '.js-site-header' );
    site.elems.$smoothSlider = $( '.js-smooth-slider' );

    var $body = site.elems.$body,
        $header = site.elems.$header,
        $smoothSlider = site.elems.$smoothSlider;

    // Replaces <img> tags with injected SVG markup
    SVGInjector( $( '.js-svg-inject' ).get() );

    $body
        .on( 'click', '.js-btn-nav', function() {
            $header.toggleClass( 'is-nav-active' );
            $( [ this, '.js-nav-primary' ] ).toggleClass( 'is-active' );
        } );

    if ( $smoothSlider.length ) {

        $smoothSlider.smoothSlider( {
            sliderDuration: 7000
        } );

    }

} );