'use strict';

jQuery(function ($) {

    var $body = site.elems.$body,
        isotope,
        $isotope = $('.js-isotope-people');

    if ($isotope.length) {

        isotope = $isotope.addClass('is-active').isotope({
            'itemSelector': '.js-person',
            'layoutMode': 'packery',
            'percentPosition': true,
            'packery': {
                'columnWidth': '.js-sizer',
                'gutter': '.js-gutter'
            }
        });

        $isotope.find('.js-person').on('click', function(e) {
            var $target = $(this);
            e.preventDefault();
            $(this).addClass('is-active');
            $isotope.isotope().one('arrangeComplete', function() {
                $.scrollTo($target, 500);
            });
        });

    }

});