'use strict';

jQuery(function ($) {

    var $body = site.elems.$body,
        isotope,
        $isotope = $('.js-isotope-posts');

    if ($isotope.length) {

        isotope = $isotope.addClass('is-active').isotope({
            'itemSelector': '.js-post',
            'layoutMode': 'packery',
            'percentPosition': true,
            'packery': {
                'columnWidth': '.js-sizer',
                'gutter': '.js-gutter'
            }
        });

    }

});