'use strict';

(function( $ ) {

    var transitionEnd = (function( transition ) {
        var transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',// Saf 6, Android Browser
            'MozTransition':    'transitionend',      // only for FF < 15
            'transition':       'transitionend'       // IE10, Opera, Chrome, FF 15+, Saf 7+
        };

        return Modernizr.csstransitions && transEndEventNames[ transition ];
    })( Modernizr.prefixed( 'transition' ) );

    var sliderStore = [];

    function Slider( $slider, $slides, opts ) {

        var $prevSlide,
            $currentSlide,
            nextTimeout;

        function setSlide() {

            $slider.find( opts.slideEventClass ).not( $prevSlide ).removeClass( opts.slideActiveClass );
            if ( transitionEnd ) {
                $currentSlide.one( transitionEnd, setHoldBackground );
            }
            else {
                setHoldBackground();

            }
            $currentSlide.css( 'z-index', opts.slideActiveZIndex ).addClass( opts.slideActiveClass );

        }

        function setSlideTimeout() {

            clearTimeout( nextTimeout );
            nextTimeout = setTimeout( nextSlide, opts.slideDuration );

        }

        function setHoldBackground() {
            $slides.css( 'z-index', '' );
            if ( $prevSlide ) {
                $prevSlide.removeClass( opts.slideActiveClass );
            }
            setSlideTimeout();
        }

        function nextSlide() {

            if ( $currentSlide ) {
                $prevSlide = $currentSlide;
            }

            if ( $currentSlide && $currentSlide.next( opts.slideEventClass ).length > 0 ) {
                $currentSlide = $currentSlide.next( opts.slideEventClass );
            }
            else {
                $currentSlide = $slides.first();
            }

            setSlide( $currentSlide );

        }

        if ( $slides.length ) {
            nextSlide( $currentSlide );
        }

    }

    $.fn.smoothSlider = function( options ) {

        var opts = $.extend( {}, $.fn.smoothSlider.defaults, options );

        return this.each( function() {

            var $elems = $( this ).find( opts.slideEventClass );
            sliderStore.push( new Slider( $( this ), $elems, opts ) );

        } );

    };

    $.fn.smoothSlider.defaults = {
        slideEventClass:   '.js-slide',
        slideActiveClass:  'is-active',
        slideAnimationClass: '.js-slide__scale',
        slideActiveZIndex: 1000,
        slideDuration:     5000
    };

})( jQuery );