'use strict';

jQuery(function ($) {

if ($('.js-stage__click' ).length > 0) {
      $(window ).on('resize', function(){
          setTimeout(function(){
              $('.js-stage__click' ).each(function(){
                  $(this ).css('width', '').css('width', $(this).outerWidth());
              });
          }, 300);
      });
      $(window ).resize();
  }

  $('body').on('click tap', '.js-stage__click', function () {
      var $parent = $(this).parents('.js-stage');
      var $stages = $parent.parents('.js-stages');
      var $bg;
      var double = false;
      $('.js-stage__expand').removeClass('is-visible');
      if ($parent.hasClass('is-expanded'))
          double = true;
      $('.js-stage').removeClass('is-expanded');
      if (!double) {
          var self = this;
          $parent.addClass('is-expanded');
          setTimeout(function () {
              $(self).siblings('.js-stage__expand').addClass('is-visible');
          }, 400);
          if (parseInt($parent.attr('data-stage')) > 3 && !Modernizr.mq('max-width: 47.99em')) {
              $stages.addClass('is-shifted');
          }
          else {
              $stages.removeClass('is-shifted');
          }
      }
      else {
          $stages.removeClass('is-shifted');
      }
      $bg = $('.js-stage-bg').removeClass('is-active');
      if ($parent.hasClass('is-expanded')) {
          $bg.filter('[data-stage="' + $parent.attr('data-stage') + '"]').addClass('is-active');
      }
      return false;
  });

  $('.js-person__view-bio, .js-person__close').on('click', function(){
      $('.js-person__bio').toggleClass('is-active');
   });

});
