<?php
/*
 * Template Name: Flexible Content
 * Template Post Type: page
 */

use ClientsFirst\WordPress\Framework\Timber\Timber;

$context = Timber::get_context();

Timber::render(['flexible.twig'], $context);