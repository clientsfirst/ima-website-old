ClientsFirst Wordpress Theme Framework
======================================

Requirements
------------

* PHP 5.6+
* WordPress 4.5+

Installation instructions
-------------------------

1. Run "composer install" to install all composer dependencies
2. Run "npm install" to install all npm dependencies
3. Run "bower install" to install all Bower dependencies

Bower
-----

* Use Bower package name if the package has one.
* Otherwise, use URL of GitHub repository suffixed with ".git"
* If the package does not have a Bower package name, or is missing or has incorrect "main" JSON data, overrides will need to be placed in bower.json