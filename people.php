<?php

/* Template Name: People */

use ClientsFirst\WordPress\Framework\Timber\Timber;

$context = Timber::get_context();

Timber::render(['people.twig'], $context);