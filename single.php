<?php

use ClientsFirst\WordPress\Framework\Timber\Timber;

$context = Timber::get_context();

Timber::render(['single.twig'], $context);