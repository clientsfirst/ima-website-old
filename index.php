<?php

use ClientsFirst\WordPress\Framework\Timber\Timber;

$context = Timber::get_context();

Timber::render(['index.twig'], $context);