<?php namespace ClientsFirst\WordPress\WordPress\Content\CustomPostTypes;

class Person extends CustomPostType {

    function __construct($name = 'person', $singular_name = 'Person', $plural_name = 'People', $dashicon = 'dashicons-businessman', $args = []) {
        $this->args = [
            'supports' => ['title', 'editor', 'thumbnail', 'custom-fields', 'revisions', 'page-attributes'],
            'hierarchical' => false,
            'labels' => [
                'featured_image' => array( __( 'Photograph' ) ),
                'set_featured_image' => array( __( 'Set photograph' ) ),
                'remove_featured_image' => array( __( 'Remove photograph' ) ),
                'use_featured_image' => array( __( 'Use as photograph' ) )
            ],
            'menu_icon' => $dashicon
        ];
        $args = array_merge_recursive($args, $this->args);
        parent::__construct($this->name, $this->singular_name, $this->plural_name, $args);
    }

    function save($post_id) {
        if (get_post($post_id)->post_type == $this->name) {
            if ($parent_id = wp_is_post_revision($post_id))
                $post_id = $parent_id;

            // unhook this function so it doesn't loop infinitely
            remove_action('save_post', array($this, 'save'), 15);

            $custom_fields = get_post_custom($post_id);
            $post_title = (!empty($custom_fields[PREFIX . 'person_first_name']) && !empty($custom_fields[PREFIX . 'person_first_name']) ? $custom_fields[PREFIX . 'person_first_name'][0] . ' ' . $custom_fields[PREFIX . 'person_last_name'][0] : 'Unnamed Person');

            $args = array(
                'ID' => $post_id,
                'post_title' => $post_title,
                'post_name' => sanitize_title($post_title)
            );

            // update the post, which calls save_post again
            wp_update_post( $args );

            // re-hook this function
            add_action('save_post', array($this, 'save'), 15);
        }
    }

}