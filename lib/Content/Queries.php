<?php namespace ClientsFirst\WordPress\Content;

use ClientsFirst\WordPress\Framework\Base;

class Queries extends Base {

	function __construct() {

		$this->add_actions([
			[ 'pre_get_posts', 'pre_get_posts' ]
		]);

	}

	function pre_get_posts(\WP_Query $query) {

		if ($query->is_home() || $query->is_archive()) {
			$query->set('posts_per_page', 12);
		}

	}

}