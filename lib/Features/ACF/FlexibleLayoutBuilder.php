<?php

namespace ClientsFirst\WordPress\Features\ACF;

use ClientsFirst\WordPress\Framework\Base;
use ClientsFirst\WordPress\Framework\Timber\Timber;

class FlexibleLayoutBuilder extends Base {

	function __construct() {
		$this->add_filters( [
			[ 'acf/format_value/name=iconic_flexible_layout', 'format_value', 12 ]
		] );
	}

	function format_value( $value ) {
		foreach ( $value as &$section ) {
			if ( $section['acf_fc_layout'] == 'carousel' ) {
				$post_type = $section['post_type'];
				$num_posts = $section['max_num'];
				$tax_query = $section['taxonomies'];
				$query     = new \WP_Query( [
					'post_type'      => $post_type,
					'posts_per_page' => ! empty( $num_posts ) ? $num_posts : 5
				] );
				if ( ! empty( $tax_query ) ) {
					$query['tax_query'] = [
						[
							'field' => 'term_id',
							'terms' => $tax_query,
						]
					];
				}
				$section['posts'] = Timber::get_posts( $query );
			}
		}

		return $value;
	}

}
