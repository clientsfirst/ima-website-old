<?php

/*
 *      This file contains the theme configuration.
 *
 *      It allows custom modules to be called in place of built-in
 *      classes. Classes which replace another class must extend the
 *      replaced class.
 */

use function DI\get;
use function DI\object;

$config = [

    /*
     *  Specifies which modules should be loaded by the theme.
     *  Modules must be declared in the dependency injector configuration returned by this file.
     */

    'modules'           => [
        'Assets',
        'Css',
        'Media',
        'Twig',
        'Shortcodes',
	    'Widgets',
	    'FlexibleContentBuilder'
    ],

    'prefix'            => 'cff_',

    'theme_dir'         => get_template_directory(),

    'menu_locations'    => [
        'main' => 'Main Menu',
	    'footer' => 'Footer Menu'
    ],

    'sidebars'          => [
        [
            'name' => 'Blog Sidebar',
            'id' => 'sidebar_blog',
            'description' => 'Widget area for the left sidebar.',
            'before_widget' => '<div class="widget %1$s %2$s">',
            'after_widget' => '</div>'
        ]
    ],

    'custom_post_types' => [

    ],

    'comments' => [
        'disable_trackbacks' => true
    ],

    /*
     * This section configures the TGM Plugin Activation class, including the list of plugins to require.
     *
     * There are two versions of the configuration - standard and dev.  If the environment is set to dev, the
     * dev configuration will be merged with the standard configuration, and will take priority if there are
     * any conflicts with the standard configuration.  This allows for plugins to only be marked as required
     * or recommended if the site is in development mode.
     */

    'tgmpa'             => [
        'standard' => [
            'plugins' => [
                // This is an example of how to include a plugin from an arbitrary external source in your theme.
                [
                    'name'     => 'Advanced Custom Fields Pro',
                    // The plugin name.
                    'slug'     => 'advanced-custom-fields-pro',
                    // The plugin slug (typically the folder name).
                    //'source' => 'https://files.clients-first.co.uk/web-development/wordpress/plugins/advanced-custom-fields-pro.zip',
                    'source'   => 'http://connect.advancedcustomfields.com/index.php?p=pro&a=download&k=b3JkZXJfaWQ9Mzk2ODV8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE0LTA5LTEzIDIwOjA3OjQ5',
                    // The plugin source.
                    'required' => true,
                    // If false, the plugin is only 'recommended' instead of required.
                ],
                [
                    'name'     => 'Better Search Replace',
                    'slug'     => 'better-search-replace',
                    'required' => false,
                ],
                [
                    'name'     => 'Bulk Creator',
                    'slug'     => 'bulk-creator',
                    'required' => false,
                ],
                [
                    'name'     => 'Duplicate Post',
                    'slug'     => 'duplicate-post',
                    'required' => true,
                ],
                [
                    'name'     => 'Ninja Forms',
                    'slug'     => 'ninja-forms',
                    'required' => true,
                ],
                [
                    'name'     => 'Query Monitor',
                    'slug'     => 'query-monitor',
                    'required' => false,
                ],
                [
                    'name'     => 'Widget CSS Classes',
                    'slug'     => 'widget-css-classes',
                    'required' => true,
                ],
                [
                    'name'     => 'Yoast SEO',
                    'slug'     => 'wordpress-seo',
                    'required' => true,
                ],
                [
                    'name'     => 'WP Smush',
                    'slug'     => 'wp-smushit',
                    'required' => true
                ]
            ],
            'config'  => [
                'id'           => 'cff_tgmpa',
                // Unique ID for hashing notices for multiple instances of TGMPA.
                'default_path' => '',
                // Default absolute path to bundled plugins.
                'menu'         => 'tgmpa-install-plugins',
                // Menu slug.
                'parent_slug'  => 'themes.php',
                // Parent menu slug.
                'capability'   => 'edit_theme_options',
                // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
                'has_notices'  => true,
                // Show admin notices or not.
                'dismissable'  => true,
                // If false, a user cannot dismiss the nag message.
                'dismiss_msg'  => '',
                // If 'dismissable' is false, this message will be output at top of nag.
                'is_automatic' => true,
                // Automatically activate plugins after installation or
            ]
        ],
        'dev' => [
            'plugins' => [
                [
                    'name'     => 'Query Monitor',
                    'slug'     => 'query-monitor',
                    'required' => false,
                ],
                [
                    'name'     => 'Simply Show Hooks',
                    'slug'     => 'simply-show-hooks',
                    'required' => true
                ]
            ]
        ]
    ],

    /*
     *  This section controls how the Wordpress asset enqueue system functions when this theme is active
     *  To allow the system to function as normal, set 'exclude->enqueued' to false
     *  To whitelist assets so that only those handles present in 'exclude->handles' are output, set 'exclude->enqueued' to true
     *  To blacklist assets so that only those handles not present in 'exclude->handles' are output, set 'exclude->enqueued' to 'blacklist'
     */

    'scripts' => [
        'exclude' => [
            'enqueued' => 'blacklist',
            'handles'  => [
            	'jquery-core',
	            'jquery-migrate'
            ]
        ]
    ],
    'styles'  => [
        'exclude' => [
            'enqueued' => false,
            'handles'  => [

            ]
        ]
    ],

    'widgets' => [
        ClientsFirst\WordPress\Framework\Content\Widgets\HTML::class
    ]

];

return [

    // Dependency Injector Container

    'Container' => object( DI\Container::class ),

    // Core Dependencies

    'Config' => object( Dflydev\DotAccessData\Data::class )
        ->constructor( $config ),

    'Env' => object( Dotenv\Dotenv::class )
        ->constructor( $config['theme_dir'] )
        ->method( 'overload' ),

    'Mobile_Detect' => object( \Detection\MobileDetect::class ),

    'Twig' => object( ClientsFirst\WordPress\Framework\Timber\Twig::class ),

    // Core Theme Modules - these must be loaded, either through the default class, or an extended replacement class

    'Core' => object( ClientsFirst\WordPress\Framework\Core::class )
        ->constructor( get( 'Config' ) ),

    // Optional Theme Modules

    'Assets' => object( ClientsFirst\WordPress\Framework\Content\Assets::class )
		->constructor( get( 'Config' ) ),

    'Css' => object( ClientsFirst\WordPress\Framework\Templates\Css::class ),

    'Media' => object( ClientsFirst\WordPress\Framework\Content\Media::class ),

    'Widgets' => object( ClientsFirst\WordPress\Framework\Content\Widgets::class )
		->constructor( get( 'Config' ) ),

    'Shortcodes' => object( ClientsFirst\WordPress\Framework\Content\Shortcodes::class ),

	'FlexibleContentBuilder' => object( ClientsFirst\WordPress\Features\ACF\FlexibleLayoutBuilder::class )

];